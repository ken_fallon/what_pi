## Overview

This is a simple script I wrote for myself to help me keep an inventory of my
Raspberry Pi collection.

It uses info from `/proc/cpuinfo` and `vgencmd` and a lookup table from
`http://elinux.org/RPi_HardwareHistory` (which has to be supplied by the
user).

## Usage

Just run it on a Raspberry Pi. So far I have tested it on an original model B,
a B+, a Pi 2 and a Pi 3. It's also been used on all versions of the Zero to
date.

```
$ ./what_pi
Revision     : a01041
Release date : Q1 2015
Model        : 2 Model B
PCB Revision : 1.1
Memory       : 1 GB
Notes        : (Mfg by Sony)
Serial no    : 00000000123a456d

Various configuration and other settings:
CPU temp=32.6'C
H264:   H264=enabled
MPG2:   MPG2=disabled
WVC1:   WVC1=disabled
MPG4:   MPG4=enabled
MJPG:   MJPG=enabled
WMV9:   WMV9=disabled
sdtv_mode=0
sdtv_aspect=0

Network information:
  Hostname   : rpi4
  IP         : 192.168.0.62
  MAC        : b8:27:eb:0a:73:2d (eth0)
```

<!--
vim: syntax=markdown:ts=8:sw=4:ai:et:tw=78:fo=tcqn:fdm=marker
-->

